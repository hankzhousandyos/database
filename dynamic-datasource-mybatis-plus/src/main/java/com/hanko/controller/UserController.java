package com.hanko.controller;

import com.hanko.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: hanko
 * @Date: 2020.11.11 17:23
 */
@RestController
public class UserController {
    @Resource
    UserService userService;

    @GetMapping("/user/{type}")
    public String user(@PathVariable  String type){
        String result;
        if("S".equals(type)){
            result = userService.selectByCondition().toString();
        }else {
            result = userService.selectAll().toString();
        }
        return result;
    }
}
