package com.hanko.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Author: hanko
 * @Date: 2020.11.11 14:19
 */

public interface UserService extends IService {
    public List selectAll();
    public List selectByCondition();
}
