package com.hanko.service.impl;/**
 * @Author: hanko
 * @Date: 2020.11.11 14:21
 */

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hanko.mapper.UserMapper;
import com.hanko.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DS("master")
public class UserServiceImpl extends ServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List selectAll() {
        return  userMapper.selectList(null);
    }

    @Override
    @DS("slave_1")
    public List selectByCondition() {
        return  userMapper.selectList(null);
    }
}