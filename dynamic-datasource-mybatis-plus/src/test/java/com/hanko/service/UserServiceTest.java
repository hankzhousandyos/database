package com.hanko.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: hanko
 * @Date: 2020.11.11 14:37
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceTest {

    @Autowired
    UserService userService;
    @Test
    void selectAll() {
        List list = userService.selectAll();
        System.out.println("list = " + list);
    }

    @Test
    void selectByCondition() {
        List list = userService.selectByCondition();
        System.out.println("list = " + list);
    }
}