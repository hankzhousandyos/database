package com.hanko.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hanko.entity.User;
import com.hanko.model.MyPage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * MP 支持不需要 UserMapper.xml 这个模块演示内置 CRUD 咱们就不要 XML 部分了
 * </p>
 *
 * 3.x 的 page 可以进行取值,多个入参记得加上注解
 * 自定义 page 类必须放在入参第一位
 * 返回值可以用 IPage<T> 接收 也可以使用入参的 MyPage<T> 接收
 * <li> 3.1.0 之前的版本使用注解会报错,写在 xml 里就没事 </li>
 * <li> 3.1.0 开始支持注解,但是返回值只支持 IPage ,不支持 IPage 的子类</li>
 *
 * @author hanko
 * @param myPage 自定义 page
 * @return 分页数据
 */

public interface UserMapper extends BaseMapper<User> {
    /**
     * @param myPage
     * @return
     */
    @Select("select * from user where age = #{pg.selectInt}")
    MyPage<User> mySelectPage(@Param("pg") MyPage<User> myPage);

}
