package com.hanko;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hanko.entity.User;
import com.hanko.mapper.UserMapper;
import com.hanko.model.MyPage;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import javax.annotation.Resource;

@Slf4j
@SpringBootTest
public class UserTest {

    @Resource
    private UserMapper mapper;

    @Test
    @Transactional
    @Rollback
    public void aInsert() {
        User user = new User();
        user.setName("小羊");
        user.setAge(3);
        user.setEmail("abc@mp.com");
        assertThat(mapper.insert(user)).isGreaterThan(0);
        log.info(user.toString());
    }

    @Test
    public void dSelect() {
        mapper.selectList(Wrappers.<User>lambdaQuery().select())
                .forEach(x -> {
                    assertThat(x.getId()).isNotNull();
                    log.info(x.toString());
                });
    }


    @Test
    public void lambdaPagination() {
        Page<User> page = new Page<>(1, 3);
        Page<User> result = mapper.selectPage(page, Wrappers.<User>lambdaQuery().ge(User::getAge, 3).orderByAsc(User::getAge));
        result.getRecords().forEach(res->log.info(res.toString()));
        log.info("Total="+result.getTotal());
    }

    @Test
    public void wrappers_test() {
       Page page = new Page(1,3);
        Page<User> result = mapper.selectPage(page,Wrappers.<User>lambdaQuery().eq(User::getAge,18));
        result.getRecords().forEach(res->log.info(res.toString()));
    }

    @Test
    public void mySelectPage() {
        MyPage myPage = new MyPage(1,3);
        //对应mapper中的参数
        myPage.setSelectInt(18);
        MyPage<User>  result = mapper.mySelectPage(myPage);
        result.getRecords().forEach(res->log.info(res.toString()));
    }

}
