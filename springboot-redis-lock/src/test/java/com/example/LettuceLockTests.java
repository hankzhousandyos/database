package com.example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LettuceLockTests {

    @Autowired
    LettuceLock lettuceLock;

    @Test
    void lock() {
        lettuceLock.tryLock("lettuce_lock", "test", 60);
    }

}
