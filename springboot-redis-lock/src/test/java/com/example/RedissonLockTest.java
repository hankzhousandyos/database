package com.example;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: hanko
 * @Date: 2021-9-22 19:31
 */
@SpringBootTest
class RedissonLockTest {
    @Autowired
    RedissonLock redissonLock;


    @Test
    void lock() {
        redissonLock.lock();
    }


    @SneakyThrows
    @Test
    void lock_thread() {
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                redissonLock.lock();
            }).start();

            TimeUnit.SECONDS.sleep(1);
        }
    }
}
